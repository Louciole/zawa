function initCursor(){
    const cursorBig = document.querySelector('.cursor');
    const initX = parseInt(getComputedStyle(cursorBig).left.split('px')[0])
    const initY = parseInt(getComputedStyle(cursorBig).top.split('px')[0])

    const positionElement = (e)=> {
        const mouseY = e.clientY;
        const mouseX = e.clientX;

        cursorBig.style.left=`${initX + mouseX}px`;
        cursorBig.style.top=`${initY + mouseY + window.scrollY}px`;
        // cursorBig.style.transform = `translate3d(${initX + mouseX}px, ${initY + mouseY + window.scrollY}px, 0)`;

    }
    window.addEventListener('mousemove', positionElement)
}
