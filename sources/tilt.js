let tiltObject

function initTilt(){
    tiltObject = document.getElementById('tilting');
}

document.addEventListener('mousemove', (event) => {
    var px = Math.abs(Math.floor(100 / tiltObject.offsetWidth * event.clientX)-100);
    var py = Math.abs(Math.floor(100 / tiltObject.offsetHeight * event.clientY)-100);
    var lp = (50+(px - 50)/1.5);
    var tp = (50+(py - 50)/1.5);
    var ty = ((tp - 50)/2) * 1;
    var tx = ((lp - 50)/1.5) * 0.5;

    tiltObject.style.transform = `rotateX(${tx}deg) rotateY(${ty}deg)`;
});